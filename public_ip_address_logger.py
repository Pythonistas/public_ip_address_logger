from datetime import datetime
import os
import requests

LOG = '/var/log/ip.log'
URL = 'https://ipinfo.io/ip'

r = requests.get(URL)
if r.status_code == 200:
    ip = r.content.decode('ascii').rstrip('\n')
    last_ip = None
    if os.path.exists(LOG):
        f = open(LOG, 'r')
        last_ip = f.readlines()[-1].split()[-1]
        f.close()
    if ip != last_ip:
        f = open(LOG, 'a')
        f.write("{} {}\n".format(datetime.now(), ip))